class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.inputDriverAvailability = document.getElementById(
      "inputDriverAvailability"
    );
    this.inputDate = document.getElementById("inputDate");
    this.inputTime = document.getElementById("inputTime");
    this.inputPassangers = document.getElementById("inputPassangers");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = async () => {
    await this.load();
    this.clear();

    const node = document.createElement("div");
    node.className = "row";
    this.carContainerElement.className = "container";
    const baru = document.createElement("div");
    baru.className = "container";
    node.appendChild(baru);

    const baris = document.createElement("div");
    baris.className = "row";
    this.carContainerElement.appendChild(baris);

    const driverAvailabilityValue = this.inputDriverAvailability.value;
    Car.list
      .filter((car) => {
        if (car.available === Boolean(driverAvailabilityValue)) {
          console.log(car);
          return car;
        }
      })
      .map((car) => {
        const col = document.createElement("div");
        col.className = "col-sm-4";
        col.innerHTML = car.render();
        baris.appendChild(col);
      });
  };

  async load() {
    const inputDateValue = this.inputDate.value;
    const inputTimeValue = this.inputTime.value;
    const inputPassangersValue = this.inputPassangers.value;

    console.log("Date", inputDateValue);
    console.log("Time", inputTimeValue);
    console.log("jmlhPenumpang", inputPassangersValue);

    const dateTime = new Date(`${inputDateValue} ${inputTimeValue}`);
    console.log("Date Time:", dateTime);

    const epochTime = dateTime.getTime();
    console.log("epochTime:", epochTime);

    const cars = await Binar.listCars((item) => {
      const showPenumpang = item.capacity == inputPassangersValue;
      const showDateTime = item.availableAt.getTime() < epochTime;
      return showPenumpang && showDateTime;
    });
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
