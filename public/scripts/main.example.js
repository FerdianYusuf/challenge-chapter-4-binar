/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();
app.loadButton.onclick = app.load();
const btnLoad = app.loadButton;
btnLoad.addEventListener("click", function (e) {
  e.preventDefault();
  app.load().then(app.run);
});

// app.init().then(app.run);
